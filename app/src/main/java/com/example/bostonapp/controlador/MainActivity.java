package com.example.bostonapp.controlador;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.bostonapp.R;
import com.example.bostonapp.modelo.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    EditText user, pass;
    Button boton;

    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        user = (EditText) findViewById(R.id.txtUser);
        pass = (EditText) findViewById(R.id.txtPass);
        boton = (Button) findViewById(R.id.btnLogin);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, user.getText().toString()+" "+pass.getText().toString(), Toast.LENGTH_SHORT).show();
                //validarUsuario("http://192.168.100.103/bostonappwebservice/validaUsuario.php");
                validarUsuario("http://10.2.15.69/bostonappwebservice/validaUsuario.php");
            }
        });
    }

    private void validarUsuario(String URL) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                User usuario = new User();

                if (response.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Usuario o contraseña incorrecto/s", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        usuario.setDni(jsonObject.optInt("dni"));
                        usuario.setCuenta_user(jsonObject.optString("cuenta_user"));
                        usuario.setNombre_user(jsonObject.optString("nombre_user"));
                        usuario.setApellido_user(jsonObject.optString("apellido_user"));
                        usuario.setDireccion_user(jsonObject.optString("direccion_user"));
                        usuario.setTelefono(jsonObject.optString("telefono"));
                        usuario.setId_privilegio(jsonObject.optInt("id_privilegio"));
                        usuario.setHabilitado(jsonObject.optInt("habilitado"));
                        usuario.setRoot(jsonObject.optInt("root"));
                        Intent intent = new Intent(getApplicationContext(), InicioActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("usuario", usuario);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "ERROR DE CONEXION", Toast.LENGTH_SHORT).show();
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parametros = new HashMap<String, String>();
                parametros.put("usuario", user.getText().toString());
                parametros.put("password", pass.getText().toString());
                return parametros;

            }

        };

            requestQueue =Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        }
    }
