package com.example.bostonapp.controlador;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bostonapp.R;
import com.example.bostonapp.adaptador.Tipo1Adapter2;
import com.example.bostonapp.modelo.Tipo1;

import java.util.ArrayList;
import java.util.List;

public class TraerTipo1Activity2 extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    private List<Tipo1> tipo1List;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cargatipo1);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        tipo1List = new ArrayList<>();

        for (int i = 0; i<=10; i++){
            Tipo1 tipo1 = new Tipo1();
            tipo1.setId_tipoProd1(1);
            tipo1.setDesc_tipoProd1("Prueba");

            tipo1List.add(tipo1);

        }

        adapter = new Tipo1Adapter2(tipo1List, this);
        recyclerView.setAdapter(adapter);



    }
}
