package com.example.bostonapp.adaptador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.bostonapp.R;
import com.example.bostonapp.modelo.Tipo1;

import java.util.List;

public class Tipo1Adapter extends RecyclerView.Adapter<Tipo1Adapter.ProductViewHolder> {


    private Context mCtx;
    private List<Tipo1> productList;

    public Tipo1Adapter(Context mCtx, List<Tipo1> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.activity_listaprod1, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Tipo1 tipo1 = productList.get(position);

        //loading the image

        /*Glide.with(mCtx)
                .load(tipo1.getImagen_tipoProd1())
                .into(holder.imageView);*/

        holder.txt_id_tipoProd1.setText(tipo1.getId_tipoProd1());
        holder.txt_desc_tipoProd1.setText(tipo1.getDesc_tipoProd1());

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView txt_id_tipoProd1, txt_desc_tipoProd1;
        ImageView imageView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            //txt_desc_tipoProd1 = itemView.findViewById(R.id.textViewTitle);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
