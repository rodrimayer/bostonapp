package com.example.bostonapp.controlador;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.bostonapp.R;
import com.example.bostonapp.adaptador.Tipo1Adapter;
import com.example.bostonapp.adaptador.Tipo1Adapter2;
import com.example.bostonapp.modelo.Tipo1;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TraerTipo1Activity extends AppCompatActivity {

    private static final String TIPO1_URL = "http://10.2.15.69/bostonappwebservice/traertipo1.php";

    RecyclerView recyclerView;
    Tipo1Adapter2 adapter;
    Context context;

    List<Tipo1> tipo1List;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cargatipo1);

        tipo1List = new ArrayList<>();
        context = this;

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        cargarTipo1();
    }

    private void cargarTipo1() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando datos...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, TIPO1_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONArray tipoProd1 = new JSONArray(response);
                            //Tipo1 tipo1 = new Tipo1();

                            for (int i = 0; i < tipoProd1.length(); i++) {
                                JSONObject tipoProdObject = tipoProd1.getJSONObject(i);

                                int id = tipoProdObject.getInt("id_tipoProd1");
                                String descTipoProd1 = tipoProdObject.getString("desc_tipoProd1");
                                //Toast.makeText(TraerTipo1Activity.this, id+" "+descTipoProd1, Toast.LENGTH_SHORT).show();
                                String imagenTipoProd1 = tipoProdObject.getString("imagen_tipoProd1");

                                Tipo1 tipo1 = new Tipo1();
                                tipo1.setId_tipoProd1(id);
                                tipo1.setDesc_tipoProd1(descTipoProd1);
                                tipo1.setImagen_tipoProd1(imagenTipoProd1);

                                Log.d("muestreo", id+" "+descTipoProd1);
                                tipo1List.add(tipo1);
                            }

                            adapter = new Tipo1Adapter2(tipo1List, getApplicationContext());
                            recyclerView.setAdapter(adapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(TraerTipo1Activity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

        });

        Volley.newRequestQueue(this).add(stringRequest);
    }


}
