package com.example.bostonapp.modelo;

import java.io.Serializable;

public class User implements Serializable {
    private int dni;
    private String cuenta_user;
    private String pass_user;
    private String nombre_user;
    private String apellido_user;
    private String direccion_user;
    private String telefono;
    private int id_privilegio;
    private int habilitado;
    private int root;

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getCuenta_user() {
        return cuenta_user;
    }

    public void setCuenta_user(String cuenta_user) {
        this.cuenta_user = cuenta_user;
    }

    public String getPass_user() {
        return pass_user;
    }

    public void setPass_user(String pass_user) {
        this.pass_user = pass_user;
    }

    public String getNombre_user() {
        return nombre_user;
    }

    public void setNombre_user(String nombre_user) {
        this.nombre_user = nombre_user;
    }

    public String getApellido_user() {
        return apellido_user;
    }

    public void setApellido_user(String apellido_user) {
        this.apellido_user = apellido_user;
    }

    public String getDireccion_user() {
        return direccion_user;
    }

    public void setDireccion_user(String direccion_user) {
        this.direccion_user = direccion_user;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getId_privilegio() {
        return id_privilegio;
    }

    public void setId_privilegio(int id_privilegio) {
        this.id_privilegio = id_privilegio;
    }

    public int getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(int habilitado) {
        this.habilitado = habilitado;
    }

    public int getRoot() {
        return root;
    }

    public void setRoot(int root) {
        this.root = root;
    }
}
