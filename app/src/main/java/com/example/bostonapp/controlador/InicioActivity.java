package com.example.bostonapp.controlador;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.bostonapp.R;
import com.example.bostonapp.adaptador.Tipo1Adapter2;
import com.example.bostonapp.modelo.Tipo1;
import com.example.bostonapp.modelo.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class InicioActivity extends AppCompatActivity {

    //public static final User user = new User();
    TextView bienv;
    CardView cardViewNuevaMesa;
    CardView cardViewRevisarMesa;
    CardView cardViewCierreCaja;
    CardView cardViewReportes;

    private static final String TIPO1_URL = "http://10.2.15.69/bostonappwebservice/traertipo1.php";

    RecyclerView recyclerView;
    Tipo1Adapter2 adapter;
    Context context;
    List<Tipo1> tipo1List;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        bienv = findViewById(R.id.txtUser);

        Bundle usuarioEnviado = getIntent().getExtras();
        User user = new User();
        tipo1List = new ArrayList<>();

        if(usuarioEnviado!=null){
            user = (User) usuarioEnviado.getSerializable("usuario");
            bienv.setText("Bienvenid@"+" "+user.getNombre_user()+" "+user.getApellido_user());
        }

        /*
        cardViewNuevaMesa = (CardView) findViewById(R.id.cardNuevaMesa);
        cardViewRevisarMesa = (CardView) findViewById(R.id.cardRevisarMesa);
        cardViewCierreCaja = (CardView) findViewById(R.id.cardCierreCaja);
        cardViewReportes = (CardView) findViewById(R.id.cardReportes);*/

        findViewById(R.id.cardNuevaMesa).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TraerTipo1Activity.class);
                startActivity(intent);
                Log.d("muestreo", "ingreso por aqui hijos de putaaaa");
                /*
                StringRequest stringRequest = new StringRequest(Request.Method.GET, TIPO1_URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                //progressDialog.dismiss();
                                try {
                                    JSONArray tipoProd1 = new JSONArray(response);
                                    Log.d("TAMAÑO ARRAY", String.valueOf(tipoProd1.length()));
                                    Tipo1 tipo1 = new Tipo1();

                                    for (int i = 0; i < tipoProd1.length(); i++) {
                                        JSONObject tipoProdObject = tipoProd1.getJSONObject(i);

                                        int id = tipoProdObject.getInt("id_tipoProd1");
                                        String descTipoProd1 = tipoProdObject.getString("desc_tipoProd1");
                                        Toast.makeText(getApplicationContext(), id+" "+descTipoProd1, Toast.LENGTH_SHORT).show();
                                        Log.d("muestreo", id+" "+descTipoProd1);
                                        //String imagenTipoProd1 = tipoProdObject.getString("imagen_tipoProd1");


                                        tipo1.setId_tipoProd1(id);
                                        tipo1.setDesc_tipoProd1(descTipoProd1);
                                        //tipo1.setImagen_tipoProd1(imagenTipoProd1);


                                        tipo1List.add(tipo1);
                                    }

                                    adapter = new Tipo1Adapter2(tipo1List, getApplicationContext());
                                    recyclerView.setAdapter(adapter);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("muestreo", "ERROOOOOOOOOOOOR");
                        //progressDialog.dismiss();
                    }

                });

                Volley.newRequestQueue(getApplicationContext()).add(stringRequest);*/
            }

        });


    }
}