package com.example.bostonapp.adaptador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.bostonapp.R;
import com.example.bostonapp.modelo.Tipo1;
import com.squareup.picasso.Picasso;

import java.util.ConcurrentModificationException;
import java.util.List;

public class Tipo1Adapter2 extends RecyclerView.Adapter<Tipo1Adapter2.ViewHolder> {

    private List<Tipo1> list;
    private Context context;
    //RequestOptions options ;

    public Tipo1Adapter2(List<Tipo1> list, Context context) {
        this.list = list;
        this.context = context;
        /*options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.activity)
                .error(R.drawable.loading);*/
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_listaprod1, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Tipo1 tipo1 = list.get(position);
        holder.textViewTipo.setText(tipo1.getDesc_tipoProd1());
        //Picasso.get().load(tipo1.getImagen_tipoProd1()).into(holder.tipoImage);
        //Glide.with(context).load(tipo1.getImagen_tipoProd1()).apply(options).into(holder.tipoImage);

        Glide.with(context)
                .load(tipo1.getImagen_tipoProd1())
                .into(holder.tipoImage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView textViewTipo;
        public ImageView tipoImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewTipo = (TextView) itemView.findViewById(R.id.textViewTipo);
            tipoImage = itemView.findViewById(R.id.tipoImage);
        }
    }
}
