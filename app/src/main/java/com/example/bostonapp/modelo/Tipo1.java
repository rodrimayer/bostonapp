package com.example.bostonapp.modelo;

import java.io.Serializable;

public class Tipo1 implements Serializable {

    private int id_tipoProd1;
    private String desc_tipoProd1;
    private String imagen_tipoProd1;

    public int getId_tipoProd1() {
        return id_tipoProd1;
    }

    public void setId_tipoProd1(int id_tipoProd1) {
        this.id_tipoProd1 = id_tipoProd1;
    }

    public String getDesc_tipoProd1() {
        return desc_tipoProd1;
    }

    public void setDesc_tipoProd1(String desc_tipoProd1) {
        this.desc_tipoProd1 = desc_tipoProd1;
    }

    public String getImagen_tipoProd1() {
        return imagen_tipoProd1;
    }

    public void setImagen_tipoProd1(String imagen_tipoProd1) {
        this.imagen_tipoProd1 = imagen_tipoProd1;
    }
}
